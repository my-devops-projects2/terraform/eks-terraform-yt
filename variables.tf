variable "kubernetes_version" {
  type = string
  default     = "1.30"
  description = "kubernetes version"
}

variable "vpc_cidr" {
  type = string
  default     = "10.0.0.0/16"
  description = "default CIDR range of the VPC"
}

variable "aws_region" {
  type = string
  default = "ap-southeast-1"
  description = "aws region"
}

variable "aws_vpc_private_subnets" {
  type = list
  default = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

variable "aws_vpc_public_subnets" {
  type = list
  default = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
}