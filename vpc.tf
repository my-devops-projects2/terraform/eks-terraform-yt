
data "aws_availability_zones" "available" {}

locals {
  cluster_name = "melz-eks-cluster"
  key_name = "melz-eks-key"
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.8.1"

  name                 = "melz-eks-vpc"
  cidr                 = var.vpc_cidr
  azs                  = data.aws_availability_zones.available.names
  private_subnets      = var.aws_vpc_private_subnets
  public_subnets       = var.aws_vpc_public_subnets
  enable_nat_gateway   = true
  single_nat_gateway   = true
#   enable_dns_hostnames = true
#   enable_dns_support   = true

  tags = {
    #"kubernetes.io/cluster/${local.cluster_name}" = "shared"
    env = "dev"
  }

  public_subnet_tags = {
    #"kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                      = "1"
  }

  private_subnet_tags = {
    #"kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"             = "1"
  }
}