# Create EKS Cluster with VPC using Terraform | Real Time Terraform Modules Implementation

[YouTube](https://www.youtube.com/watch?v=_BTpd2oYafM)

## Install AWS CLI

https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html

## Install Terraform

https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli

