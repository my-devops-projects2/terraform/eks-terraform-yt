terraform {
  required_version = "~> 1.5"
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.52.0"
    }
  }
  backend "s3" {
    encrypt = true
    bucket = "melvincv2024-opentofu-state"
    key    = "projects/eks-opentofu.tfstate"
    region = "ap-southeast-1"
    profile = "opentofu"
    dynamodb_table = "opentofu-state-lock"
  }
}

provider "aws" {
  region = var.aws_region
  profile = "opentofu"
}
